on 'https://amuse%.apiote%.xyz/login' {
	form 'me' {
		method = 'POST',
		action = '/login',
		input {
			id = 'username', name = 'username', type = 'text',
			value = 'me',
		},
		input {
			id = 'password', name = 'password', type = 'password',
			value = nextpass('Amuse', 'me'),
		},
		submit = true,
		autofill = true,
	},
}

on 'https://notabug%.org/user/login' {
	form {
		method = 'post',
		action = '/user/login',
		input {
			id = 'user_name', name = 'user_name',
			value = 'apiote',
		},
		input {
			id = 'password', name = 'password', type = 'password',
			value = nextpass('NotABug', 'apiote'),
		},
		submit = true,
		autofill = true,
	},
}

on 'https://cloud%.apiote%.xyz/login$' {
  form {
    method = 'post',
    action = '/login',
    name = 'login',
    input {
      id = 'user', name = 'user', type = 'text',
      value = 'me',
    },
    input {
      id = 'password', name = 'password', type = 'password',
      value = nextpass('Nextcloud', 'me'),
    },
    submit = true,
    autofill = true,
  },
}

on 'https://webmail%.migadu%.com/$' {
  form {
    action = '#/',
    input {
      id = 'RainLoopEmail', name = 'RainLoopEmail', type = 'email',
      value = 'me@apiote.xyz',
    },
    input {
      id = 'RainLoopPassword', name = 'RainLoopPassword', type = 'password',
      value = nextpass('me@apiote.xyz', 'me'),
    },
    input {
      type = 'text',
      value = '',
    },
    submit = true,
    autofill = true,
  },
}

on 'https://funk%.apiote%.xyz/$' {
  form {
    input {
      name = 'username', type = 'text',
      value = 'me',
    },
    input {
      name = 'password', type = 'password',
      value = nextpass('Funkwhale', 'io'),
    },
    submit = true,
    autofill = true,
  },
}

on 'https://honk%.apiote%.tk/login' {
  form {
    method = 'POST',
    action = '/dologin',
    input {
      name = 'username', type = 'text',
      value = 'me',
    },
    input {
      name = 'password', type = 'password',
      value = nextpass('Honk', 'me'),
    },
    submit = true,
    autofill = true,
  },
}

on 'https://rss%.apiote%.xyz/$' {
  form {
    method = 'post',
    action = '/login',
    input {
      id = 'form-username', name = 'username', type = 'text',
      value = 'me',
    },
    input {
      id = 'form-password', name = 'password', type = 'password',
      value = nextpass('Miniflux', 'me'),
    },
    submit = true,
    autofill = true,
  },
}

on 'https://pixelfed%.uno/login$' {
  form {
    method = 'POST',
    action = 'https://pixelfed.uno/login',
    id = 'login_form',
    input {
      id = 'email', name = 'email', type = 'email',
      value = 'pixelfed@apiote.xyz',
    },
    input {
      id = 'password', name = 'password', type = 'password',
      value = nextpass('Pixelfed', '@apiote@pixelfed.uno'),
    },
    input {
      name = 'remember', type = 'checkbox',
      checked = false,
    },
    submit = true,
    autofill = true,
  },
}

on 'https://apiote%.xyz/s/$' {
  form {
    method = 'POST',
    input {
      id = 'url', name = 'url', type = 'url',
      value = '',
    },
    input {
      id = 'password', name = 'password', type = 'password',
      value = nextpass('Shortener', ''),
    },
    input {
      id = 'id', name = 'id',
      value = '',
    },
    submit = false,
    autofill = true,
  },
}

on 'https://house%.apiote%.xyz/login' {
  form {
    method = 'post',
    action = 'https://house.apiote.xyz/login',
    id = 'login-form',
    input {
      id = 'username', name = 'username', type = 'text',
      value = 'me',
    },
    input {
      id = 'password', name = 'password', type = 'password',
      value = nextpass('Grocy', 'me'),
    },
    input {
      id = 'stay_logged_in', name = 'stay_logged_in', type = 'checkbox',
      checked = false,
    },
    submit = true,
    autofill = true,
  },
}

on 'https://money%.apiote%.xyz/login' {
  form {
    method = 'post',
    action = 'https://money.apiote.xyz/login',
    input {
      name = 'email', type = 'email', 
      value = 'firefly@apiote.xyz',
    },
    input {
      name = 'password', type = 'password', 
      value = nextpass('Firefly III', 'firefly@apiote.xyz'),
    },
    input {
      name = 'remember', type = 'checkbox', 
      checked = false,
    },
    submit = true,
    autofill = true,
  },
}
on 'https://monitoring%.apiote%.xyz/login' {
  form {
    input {
      name = 'user', 
      value = 'me',
    },
    input {
      id = 'current-password', name = 'password', type = 'password', 
      value = nextpass('Grafana', 'me'),
    },
    submit = true,
    autofill = true,
  },
}

