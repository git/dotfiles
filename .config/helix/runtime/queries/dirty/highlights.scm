[
  "("
  ")"
] @punctuation.bracket
(string) @string
(raw_string) @string
[
  (true)
  (false)
] @constant.builtin.boolean
(null) @constant.builtin
;(number) @constant.numeric
(escape_sequence) @constant.character.escape
(ERROR) @error
(comment) @comment
