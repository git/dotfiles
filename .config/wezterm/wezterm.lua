-- https://gogh-co.github.io/Gogh/

local wezterm = require 'wezterm';

return {
	default_prog = { '/usr/bin/fish', '-l' },
	font = wezterm.font_with_fallback {
		"Iosevka",
		"Noto Sans Symbols",
		"Unifont"
	},
	check_for_updates = false,
	use_fancy_tab_bar = false,
	tab_bar_at_bottom = true,
	window_background_opacity = 0.95,
	check_for_updates = false,
	color_scheme = 'BirdsOfParadise',
	colors={
		foreground = '#c9ad53',
		background = '#2a1f1d',
	  cursor_fg = '#c9ad53',

		indexed = {
			[100] = '#364045',
			[108] = '#db9d3b',
			[101] = '#892b30',
			[102] = '#1c9867',
			[103] = '#db9d3b',
			[104] = '#db9d3b',
			[105] = '#892b30',
			[106] = '#317b9f',
			[107] = '#5d3636',
			[109] = '#892b30',
			[110] = '#1c9867',
			[111] = '#db9d3b',
			[112] = '#db9d3b',
			[113] = '#892b30',
			[114] = '#317b9f',
			[115] = '#5d3636'
		},
	}
}
