#!/bin/sh

. "$HOME/.config/sh/variables"
. "$XDG_CONFIG_HOME/sh/variables.wayland"
. "$XDG_CONFIG_HOME/sh/aliases"
. "$XDG_CONFIG_HOME/sh/functions"

# pgrep lock_listen || lock_listen &

swaybg -i "$HOME/Pictures/wallpapers/$(hostname).webp" -m fill &

test ! -e /tmp/wob.sock && mkfifo /tmp/wob.sock
(if ! pgrep wob
then
	while true
	do
		cat -u /tmp/wob.sock
	done | wob &
fi) &

import_gsettings \
    gtk-theme:gtk-theme-name \
    icon-theme:gtk-icon-theme-name \
    cursor-theme:gtk-cursor-theme-name

pgrep ssh-agent || (ssh-agent > /tmp/ssh-agent.sh && ssh-agent -c > /tmp/ssh-agent.fish)

# swaymsg output HDMI-A-1 bg $(ls ~/Pictures/wallpapers/encrypt/ | sort -R | tail -1) fill

pgrep mako || mako &

doas /usr/sbin/powertop --auto-tune

swayidle -w \
    timeout 600 'swaylock -f' \
    resume 'swaymsg "output * dpms on"; ~/.local/bin/bar_all_once' \
    before-sleep 'swaylock -f' &

while [ ! -e /tmp/swaybar/pipe ]
do
	:
done
pgrep bar_network || ~/.local/bin/bar_network &
pgrep bar_battery || ~/.local/bin/bar_battery &
~/.local/bin/bar_clock

bar_all_once
bt off
alsa_usb
autoBrightness &
barHider &

network_backup &
himitsud &
